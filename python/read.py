#!/usr/bin/env python3
from os import environ
from pyftdi.jtag import JtagEngine, JtagTool
from pyftdi.bits import BitSequence
JTAG_INSTR = {
  'SAMPLE'  : BitSequence('0000000101', msb=True, length=10),
  'IDCODE'  : BitSequence('0000000110', msb=True, length=10),
  'USERCODE': BitSequence('0000000111', msb=True, length=10),
  'BYPASS'  : BitSequence('1111111111', msb=True, length=10)}
url  = environ.get('FTDI_DEVICE', 'ftdi://ftdi:2232h/1')
jtag = JtagEngine(trst=True, frequency=3E6)
jtag.configure(url)
jtag.reset()
def read_idcode(jtag):
    instr = JTAG_INSTR['IDCODE']
    jtag.reset()
    jtag.write_ir(instr)
    idcode = jtag.read_dr(32)
    jtag.go_idle()
    print("  IDCODE: 0x%08x" % int(idcode))
def read_usercode(jtag):
    instr = JTAG_INSTR['USERCODE']
    jtag.write_ir(instr)
    idcode = jtag.read_dr(32)
    jtag.go_idle()
    print("USERCODE: 0x%08x" % int(idcode))
if __name__ == '__main__':
    read_idcode(jtag)
    read_usercode(jtag)
    del jtag
# altera_ep4ce6e22:  0x020f10dd 
# altera_ep4ce15e22: 0x020f30dd
