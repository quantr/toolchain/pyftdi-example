/*
	Test app to send data to a terminal monitoring a second serial port.

	To build use the following gcc statement 
	(assuming you have the d2xx library in the /usr/local/lib directory).
	gcc -o timeouts main.c -L. -lftd2xx -Wl,-rpath /usr/local/lib
*/

#include <stdio.h>
#include <assert.h>
#include "../libftd2xx/ftd2xx.h"

typedef unsigned char uint8_t;

int main(int argc, char *argv[])
{
	int        retCode = -1; // Assume failure
	FT_STATUS  ftStatus = FT_OK;
	FT_HANDLE  ftHandle = NULL;
	int        portNum = -1; // Deliberately invalid
	
	if (argc > 1)
	{
		sscanf(argv[1], "%d", &portNum);
	}
	
	if (portNum < 0)
	{
		portNum = 0;
	}
	
	ftStatus = FT_Open(portNum, &ftHandle);
	if (ftStatus != FT_OK) 
	{
		printf("FT_Open(%d) failed, with error %d.\n", portNum, (int)ftStatus);
		printf("Use lsmod to check if ftdi_sio (and usbserial) are present.\n");
		printf("If so, unload them using rmmod, as they conflict with ftd2xx.\n");
		goto exit;
	}

	assert(ftHandle != NULL);

	ftStatus = FT_ResetDevice(ftHandle);
	if (ftStatus != FT_OK) 
	{
		printf("Failure.  FT_ResetDevice returned %d.\n", (int)ftStatus);
		goto exit;
	}

	ftStatus = FT_SetUSBParameters(ftHandle, 64, 64);
	if (ftStatus != FT_OK) {
		printf("FT_SetUSBParameters\n");
		goto exit;
	}

	ftStatus = FT_SetChars(ftHandle, 0, 0, 0, 0);
	if (ftStatus != FT_OK) {
		printf( "FT_SetChars\n");
		goto exit;
	}

	ftStatus = FT_SetTimeouts(ftHandle, 1000, 1000);
	if (ftStatus != FT_OK) {
		printf("FT_SetTimeouts\n");
		goto exit;
	}

	ftStatus = FT_SetLatencyTimer(ftHandle, 10);
	if (ftStatus != FT_OK) {
		printf("FT_SetLatencyTimer\n");
	}

	// Disable flow control
	ftStatus = FT_SetFlowControl(ftHandle, FT_FLOW_NONE, 0, 0);
	if (ftStatus != FT_OK) {
		printf( "FT_SetFlowControl\n");
	}

	// Reset MPSSE controller
	ftStatus = FT_SetBitMode(ftHandle, 0, 0);
	if (ftStatus != FT_OK) {
		printf( "FT_SetBitMode\n");
	}

	// Initialize MPSSE controller
	ftStatus = FT_SetBitMode(ftHandle, 0xFF, 0x02); // All pins outputs, MPSSE
	if (ftStatus != FT_OK) {
		printf("FT_SetBitMode\n");
	}
	
	uint8_t gpio_command[] = { 0x82, 0x00, 0xFF };
	uint8_t gpio_command2[] = { 0x82, 0xff, 0xFF };
	unsigned int bytesWritten = 0;
	// 0x80: Command to set AD[7:0].
	// 0x82: Command to set AC[7:0].
	// 0x00: Output values for AD[7:0] (placeholder)
	// 0xFF: GPIO directions for AD[7:0] (1 = output)
	while(1){
		ftStatus = FT_Write(ftHandle, gpio_command, sizeof(gpio_command), &bytesWritten);
		printf("bytesWritten=%d\n", bytesWritten);
		usleep(200000);

		ftStatus = FT_Write(ftHandle, gpio_command2, sizeof(gpio_command2), &bytesWritten);
		printf("bytesWritten=%d\n", bytesWritten);
		usleep(200000);
	}

exit:
	if (ftHandle != NULL)
		FT_Close(ftHandle);

	return retCode;
}
